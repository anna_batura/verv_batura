(function() {
  $(document).ready(function(){
    $(".section-top, .section_btn-scroll").on("click","a", function (event) {
      event.preventDefault();
      var id  = $(this).attr('href');
      var top = $(id).offset().top;
      $('body,html').animate({scrollTop: top}, 1000);
    });

    var $selectionTransform = $(".section-transform ");

    $('.section-top_btn').on('click', function() {
      $selectionTransform.slideDown();
      $(".form-sign").addClass('transform');
    });
  });
})();
